<?php

/**
 * @file Rules created by the cheevos module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function cheevos_rules_condition_info() {
  return array(
    'cheevos_condition_count' => array(
      'label' => t('Create a number of posts'),
      'parameter' => array(
        'comment' => array(
          'type' => 'text',
          'label' => t('Number of posts'),
          'description' => t('The number of posts to meet this condition.'),
          'restriction' => 'input',
        ),
      ),
      'group' => t('Cheevos'),
    ),
  );
}



/**
 * Rules condition callback.
 *
 * @see cheevos_rules_condition_info()
 */
function cheevos_condition_count($pid, $type) {
  dpm($pid);
  dpm($type);
  return TRUE;
}
