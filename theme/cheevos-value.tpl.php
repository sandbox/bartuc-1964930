<?php

/**
 * @file
 * Default theme implementation to present the value on a cheevos page.
 *
 * Available variables:
 * - $sku: The SKU to render.
 * - $label: If present, the string to use as the SKU label.
 *
 * Helper variables:
 * - $product: The fully loaded product object the SKU represents.
 */
?>
<div class="cheevos-value">
  <?php if ($value): ?>
    <div class="cheevos-value-label">
      <strong><?php print t('Value'); ?></strong>
    </div>
    <div class="cheevos-value-value">
      <?php print $value; ?>
    </div>
  <?php endif; ?>
</div>
