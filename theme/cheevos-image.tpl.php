<?php

/**
 * @file
 * Default theme implementation to present the value on a cheevos page.
 *
 * Available variables:
 * - $sku: The SKU to render.
 * - $label: If present, the string to use as the SKU label.
 *
 * Helper variables:
 * - $product: The fully loaded product object the SKU represents.
 */
?>
<div class="cheevos-image">
  <?php if ($image): ?>
    <div class="cheevos-image-label">
      <strong><?php print t('Image'); ?></strong>
    </div>
    <div class="cheevos-image-value">
      <img src="<?php print $image; ?>" />
    </div>
  <?php endif; ?>
</div>
