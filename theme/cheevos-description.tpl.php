<?php

/**
 * @file
 * Default theme implementation to present the value on a cheevos page.
 *
 * Available variables:
 * - $sku: The SKU to render.
 * - $label: If present, the string to use as the SKU label.
 *
 * Helper variables:
 * - $product: The fully loaded product object the SKU represents.
 */
?>
<div class="cheevos-description">
  <?php if ($description): ?>
    <div class="cheevos-description-label">
      <strong><?php print t('Description'); ?></strong>
    </div>
    <div class="cheevos-description-value">
      <?php print $description; ?>
    </div>
  <?php endif; ?>
</div>
