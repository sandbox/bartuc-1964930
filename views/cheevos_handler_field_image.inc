<?php

/**
 * @file
 * View field handler for images on the cheevos entity.
 */

/**
 * Class implementation of the cheevos entity image class.
 */
class cheevos_handler_field_image extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['cheevos_id'] = 'cid';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);
    $options['image_style'] = array('default' => 'thumbnail');
    $options['image_link'] = array('default' => 'None (original image)');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $image_styles = image_style_options(FALSE);
    $form['image_style'] = array(
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->options['image_style'],
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    );

    $link_types = array(
      'content' => t('Content'),
      'file' => t('File'),
    );
    $form['image_link'] = array(
      '#title' => t('Link image to'),
      '#type' => 'select',
      '#default_value' => $this->options['image_link'],
      '#empty_option' => t('Nothing'),
      '#options' => $link_types,
    );
  }

  function render($values) {
    // Create variables.
    $cid = $this->get_value($values, 'cid');
    $file = file_load($values->cheevos_fid);

    // Add an image style if selected.
    if (isset($this->options['image_style'])) {
      $style_name = $this->options['image_style'];
      $image = '<img src="' . image_style_url($style_name, $file->uri) . '" />';
    }
    else {
      $image = '<img src="' . $file->uri . '" />';
    }

    // Link the image to a location determined by options set.
    if ($this->options['image_link'] == 'content') {
      return array(
        '#theme' => 'link',
        '#text' => $image,
        '#path' => 'cheevos/' . $cid,
        '#options' => array(
          'attributes' => array(),
          'html' => TRUE,
        ),
      );
    }
    elseif ($this->options['image_link'] == 'file') {
      return array(
        '#theme' => 'link',
        '#text' => $image,
        '#path' => file_create_url($file->uri),
        '#options' => array(
          'attributes' => array(),
          'html' => TRUE,
        ),
      );
    }
    else {
      return $image;
    }
  }
}
