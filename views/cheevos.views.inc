<?php
/**
 * @file
 * Providing extra functionality for the Cheevos UI via views.
 */

/**
 * Implements hook_views_data()
 */
function cheevos_views_data_alter(&$data) {
  $data['cheevos']['link_cheevos'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the cheevos.'),
      'handler' => 'cheevos_handler_link_field',
    ),
  );
  $data['cheevos']['edit_cheevos'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the cheevos.'),
      'handler' => 'cheevos_handler_edit_link_field',
    ),
  );
  $data['cheevos']['delete_cheevos'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the cheevos.'),
      'handler' => 'cheevos_handler_delete_link_field',
    ),
  );
  // This content of this field are decided based on the menu structure that
  // follows cheevos/cheevos/%cid/op
  $data['cheevos']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this cheevos.'),
      'handler' => 'cheevos_handler_cheevos_operations_field',
    ),
  );

  $data['cheevos']['cheevos_image'] = array(
    'real field' => 'fid',
    'field' => array(
      'title' => t('Image'),
      'help' => t('Display the image associated with the cheevos.'),
      'handler' => 'cheevos_handler_field_image',
    ),
  );

  $data['cheevos_user']['table']['entity type'] = 'cheevos';
  $data['cheevos_user']['table']['group']  = t('Cheevos User');

  // For other base tables, explain how we join
  $data['cheevos_user']['table']['join'] = array(
    // Directly links to cheevos table.
    'cheevos' => array(
      'left_field' => 'cid',
      'field' => 'cid',
    ),
  );

  $data['cheevos_user']['table']['default_relationship'] = array(
    'cheevos' => array(
      'table' => 'cheevos',
      'field' => 'cid',
    ),
  );

  $data['cheevos_user']['cheevos_uid'] = array(
    'title' => t('Cheevos uid'),
    'help' => t("The user's uid that has obtained cheevos."),
    'real field' => 'uid',
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
  );

  $data['cheevos_user']['cheevos_obtained'] = array(
    'real field' => 'obtained',
    'field' => array(
      'title' => t('Obtained'),
      'help' => t('When the cheevos was obtained'),
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
}
