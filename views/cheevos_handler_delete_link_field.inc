<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying deletes links
 * as fields
 */


class cheevos_handler_delete_link_field extends cheevos_handler_link_field {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }


  function render($values) {
    $type = $values->{$this->aliases['type']};

    //Creating a dummy cheevos to check access against
    $dummy_cheevos = (object) array('type' => $type);
    if (!cheevos_access('edit', $dummy_cheevos)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    $cheevos = $values->{$this->aliases['cheevos']};

    return l($text, 'admin/cheevos/content/cheevos/' . $cheevos . '/delete');
  }
}
