<?php
/**
 * @file
 * Providing default views.
 */

/**
 * Implements hook_views_default_views().
 */
function cheevos_views_default_views() {
  if (!variable_get('cheevos_plural', TRUE)) {
    $cheevos = variable_get('cheevos_plural_diff', 'Cheevos');
  }
  else {
    $cheevos = variable_get('cheevos_label', 'Cheevos');
  }
  $views = array();

  $view = new view();
  $view->name = 'cheevos_list';
  $view->description = 'A list of all cheevos';
  $view->tag = 'cheevos';
  $view->base_table = 'cheevos';
  $view->human_name = 'Cheevos List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Cheevos';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'create any cheevos type';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'cid' => 'cid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'cid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Empty ';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No cheevos have been created yet';
  /* Field: Cheevos: Cheevos ID */
  $handler->display->display_options['fields']['cid']['id'] = 'cid';
  $handler->display->display_options['fields']['cid']['table'] = 'cheevos';
  $handler->display->display_options['fields']['cid']['field'] = 'cid';
  $handler->display->display_options['fields']['cid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['cid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['cid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['cid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['cid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['cid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['cid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['cid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['cid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['cid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['cid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['cid']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['cid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['cid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['cid']['empty_zero'] = 0;
  /* Field: Cheevos: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'cheevos';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  /* Field: Cheevos: Link */
  $handler->display->display_options['fields']['link_cheevos']['id'] = 'link_cheevos';
  $handler->display->display_options['fields']['link_cheevos']['table'] = 'cheevos';
  $handler->display->display_options['fields']['link_cheevos']['field'] = 'link_cheevos';
  $handler->display->display_options['fields']['link_cheevos']['label'] = 'View';
  $handler->display->display_options['fields']['link_cheevos']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['alter']['external'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['link_cheevos']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['link_cheevos']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['alter']['html'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['link_cheevos']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['link_cheevos']['hide_empty'] = 0;
  $handler->display->display_options['fields']['link_cheevos']['empty_zero'] = 0;

  /* Field: Cheevos: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'cheevos';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['operations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['operations']['empty_zero'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'cheevos_admin_page');
  $handler->display->display_options['path'] = 'admin/cheevos/content/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['tab_options']['type'] = 'tab';
  $handler->display->display_options['tab_options']['title'] = 'Cheevos';
  $handler->display->display_options['tab_options']['description'] = 'Manage cheevos';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';
  $translatables['cheevos'] = array(
    t('Master'),
    t('Cheevos'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Empty '),
    t('No Cheevos have been created yet'),
    t('Cheevos ID'),
    t('.'),
    t(','),
    t('Name'),
    t('View'),
    t('Operations links'),
    t('Page'),
  );
  $views[$view->name] = $view;

  $view = new view();
  $view->name = 'cheevos_user';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'cheevos';
  $view->human_name = 'Cheevos Users';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'My ' . $cheevos;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view own cheevos';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'description' => 'description',
    'value' => 'value',
    'cheevos_obtained' => 'cheevos_obtained',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'description' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cheevos_obtained' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'You currently have no cheevos.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Cheevos: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'cheevos';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Cheevos: Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'cheevos';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  /* Field: Cheevos: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'cheevos';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  /* Contextual filter: Cheevos User: Cheevos uid */
  $handler->display->display_options['arguments']['cheevos_uid']['id'] = 'cheevos_uid';
  $handler->display->display_options['arguments']['cheevos_uid']['table'] = 'cheevos_user';
  $handler->display->display_options['arguments']['cheevos_uid']['field'] = 'cheevos_uid';
  $handler->display->display_options['arguments']['cheevos_uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['cheevos_uid']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['cheevos_uid']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['cheevos_uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['cheevos_uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['cheevos_uid']['summary_options']['items_per_page'] = '25';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Cheevos: Image */
  $handler->display->display_options['fields']['cheevos_image']['id'] = 'cheevos_image';
  $handler->display->display_options['fields']['cheevos_image']['table'] = 'cheevos';
  $handler->display->display_options['fields']['cheevos_image']['field'] = 'cheevos_image';
  $handler->display->display_options['fields']['cheevos_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['cheevos_image']['image_style'] = 'cheevos_list';
  $handler->display->display_options['fields']['cheevos_image']['image_link'] = '';
  /* Field: Cheevos: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'cheevos';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Cheevos: Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'cheevos';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  /* Field: Cheevos: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'cheevos';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  /* Field: Cheevos User: Obtained */
  $handler->display->display_options['fields']['cheevos_obtained']['id'] = 'cheevos_obtained';
  $handler->display->display_options['fields']['cheevos_obtained']['table'] = 'cheevos_user';
  $handler->display->display_options['fields']['cheevos_obtained']['field'] = 'cheevos_obtained';
  $handler->display->display_options['fields']['cheevos_obtained']['date_format'] = 'custom';
  $handler->display->display_options['fields']['cheevos_obtained']['custom_date_format'] = 'm-d-Y';
  $handler->display->display_options['path'] = 'user/%/cheevos';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'My ' . $cheevos;
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $views[$view->name] = $view;

  return $views;
}
