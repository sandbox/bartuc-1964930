<?php

/**
 * @file
 * Cheevos editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring
 * instead to use views. That offers more flexibility to change a UI that will,
 * more often than not, be end-user facing.
 */

/**
 * UI controller.
 */
class CheevosUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {

    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Cheevos',
      'description' => 'Add edit and update cheevos.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the overview menu type for the list of cheevos.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;

    // Change the add page menu to multiple types of entities.
    $items[$this->path . '/add'] = array(
      'title' => 'Add a cheevos',
      'description' => 'Add a new cheevos',
      'page callback'  => 'cheevos_add_page',
      'access callback'  => 'cheevos_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'cheevos.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    // Add menu items to add each different type of entity.
    foreach (cheevos_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type] = array(
        'title' => 'Add ' . $type->label,
        'page callback' => 'cheevos_form_wrapper',
        'page arguments' => array(cheevos_create(array('type' => $type->type))),
        'access callback' => 'cheevos_access',
        'access arguments' => array('edit', 'edit ' . $type->type),
        'file' => 'cheevos.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module'])
      );
    }

    // Loading and editing cheevos entities.
    $items[$this->path . '/cheevos/' . $wildcard] = array(
      'page callback' => 'cheevos_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'cheevos_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'cheevos.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items[$this->path . '/cheevos/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );

    $items[$this->path . '/cheevos/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'cheevos_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'cheevos_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'cheevos.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    // Menu item for viewing cheevos.
    $items['cheevos/' . $wildcard] = array(
      'title callback' => 'cheevos_page_title',
      'title arguments' => array(1),
      'page callback' => 'cheevos_page_view',
      'page arguments' => array(1),
      'access callback' => 'cheevos_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );
    return $items;
  }

  /**
   * Create the markup for the add Cheevos Entities page within the class
   * so it can easily be extended/overriden.
   */
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }

    return theme('cheevos_add_list', array('content' => $content));
  }

}

/**
 * Form callback wrapper: create or edit a cheevos.
 *
 * @param $cheevos
 *   The cheevos object being edited by this form.
 *
 * @see cheevos_edit_form()
 */
function cheevos_form_wrapper($cheevos) {
  // Add the breadcrumb for the form's location.
  cheevos_set_breadcrumb();
  return drupal_get_form('cheevos_edit_form', $cheevos);
}

/**
 * Form callback wrapper: delete a cheevos.
 *
 * @param $cheevos
 *   The cheevos object being edited by this form.
 *
 * @see cheevos_edit_form()
 */
function cheevos_delete_form_wrapper($cheevos) {
  // Add the breadcrumb for the form's location.
  return drupal_get_form('cheevos_delete_form', $cheevos);
}

/**
 * Form callback: create or edit a cheevos.
 *
 * @param $cheevos
 *   The cheevos object to edit or for a create form an empty cheevos object
 *   with only a cheevos type defined.
 */
function cheevos_edit_form($form, &$form_state, $cheevos) {
  // Add the default field elements.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Cheevos Name'),
    '#default_value' => isset($cheevos->name) ? $cheevos->name : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($cheevos->description) ? $cheevos->description : '',
    '#required' => TRUE,
  );

  // @todo allow the user to choose the path in the admin UI.
  $form['fid'] = array(
    '#type' => 'managed_file',
    '#title' => t('Choose an Image'),
    '#description' => t('The image must be a .png, .jpg, or .gif'),
    '#default_value' => isset($cheevos->fid) ? $cheevos->fid : '',
    '#size' => 24,
    '#upload_location' => 'public://cheevos',
    "#upload_validators" => array('file_validate_extensions' => array('png gif jpg')),
  );

  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#default_value' => isset($cheevos->value) ? $cheevos->value : '',
    '#description' => t('A numeric value for the cheevos.'),
    '#size' => 12,
    '#maxlength' => 12,
    '#required' => TRUE,
  );

  // Add the field related form elements.
  $form_state['cheevos'] = $cheevos;
  field_attach_form('cheevos', $cheevos, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save cheevos'),
    '#submit' => $submit + array('cheevos_edit_form_submit'),
  );

  if (!empty($cheevos->name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete cheevos'),
      '#suffix' => l(t('Cancel'), 'admin/cheevos/content'),
      '#submit' => $submit + array('cheevos_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'cheevos_edit_form_validate';
  return $form;
}

/**
 * Form API validate callback for the cheevos form.
 */
function cheevos_edit_form_validate(&$form, &$form_state) {
  if (!is_numeric($form_state['values']['value'])) {
    form_set_error('value', t('The value must be numeric.'));
  }
  $cheevos = $form_state['cheevos'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('cheevos', $cheevos, $form, $form_state);
}

/**
 * Form API submit callback for the cheevos form.
 *
 * @todo remove hard-coded link.
 */
function cheevos_edit_form_submit(&$form, &$form_state) {
  // Build the cheevos entity.
  $cheevos = entity_ui_controller('cheevos')->entityFormSubmitBuildEntity($form, $form_state);

  // Add in created and changed times.
  if ($cheevos->is_new = isset($cheevos->is_new) ? $cheevos->is_new : 0){
    $cheevos->created = time();
  }
  $cheevos->changed = time();

  // Save the cheevos and go back to the list of cheevos
  $cheevos->save();

  // Load the uploaded file and save it permanently.
  $file = file_load($form_state['values']['fid']);

  // Change status to permanent.
  $file->status = FILE_STATUS_PERMANENT;

  // Save.
  file_save($file);

  // Record that the module is using the file.
  file_usage_add($file, 'cheevos', $cheevos->type, $cheevos->cid);

  // Redirect the user back to the view page.
  $form_state['redirect'] = 'admin/cheevos/content';
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function cheevos_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/cheevos/content/cheevos/' . $form_state['cheevos']->cid . '/delete';
}

/**
 * Form callback: confirmation form for deleting a cheevos.
 *
 * @param $cheevos
 *   The cheevos to delete.
 *
 * @see confirm_form()
 */
function cheevos_delete_form($form, &$form_state, $cheevos) {
  $form_state['cheevos'] = $cheevos;

  $form['#submit'][] = 'cheevos_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete cheevos %name?', array('%name' => $cheevos->name)),
    'admin/content/cheevos/cheevos',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for cheevos_delete_form.
 */
function cheevos_delete_form_submit($form, &$form_state) {
  $cheevos = $form_state['cheevos'];

  cheevos_delete($cheevos);

  drupal_set_message(t('The cheevos %name has been deleted.', array('%name' => $cheevos->name)));
  watchdog('cheevos', 'Deleted cheevos %name.', array('%name' => $cheevos->name));

  $form_state['redirect'] = 'admin/cheevos/content';
}

/**
 * Menu callback form.
 */
function cheevos_settings_form($form, $form_state) {
  $form = array();

  $form['cheevos_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Cheevos Label'),
    '#description' => t('The singular label that users will see. For example in the user profile, by default, will show "My Cheevos".'),
    '#default_value' => variable_get('cheevos_label', 'Cheevos'),
  );

  $form['cheevos_plural'] = array(
    '#type' => 'checkbox',
    '#title' => t('Plural and singular'),
    '#description' => t('Can the label above be used plurally and singularly?'),
    '#default_value' => variable_get('cheevos_plural', TRUE),
  );

  $form['cheevos_plural_diff'] = array(
    '#type' => 'textfield',
    '#title' => t('Plural Wording'),
    '#description' => t('The plural version of the cheevos. Ex. Achievement would be Achievements.'),
    '#default_value' => variable_get('cheevos_plural_diff', 'Cheevos'),
    '#states' => array(
      'invisible' => array(
       ':input[name="cheevos_plural"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['cheevos_appearance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Appearance'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['cheevos_appearance']['cheevos_javascript'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display using Javascript'),
    '#description' => t('If disabled, drupal_set_message will be used.'),
    '#default_value' => variable_get('cheevos_javascript', TRUE),
  );

  $form['cheevos_appearance']['cheevos_position'] = array(
    '#type' => 'radios',
    '#title' => t('Indicator Position'),
    '#options' => array(
      0 => t('Top Left'),
      1 => t('Top Right'),
      2 => t('Bottom Left'),
      3 => t('Bottom Right'),
    ),
    '#default_value' => variable_get('cheevos_position', 2),
    '#states' => array(
      'invisible' => array(
       ':input[name="cheevos_javascript"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['cheevos_appearance']['cheevos_modal'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show cheevos in modal'),
    '#description' => t('When a user clicks on the cheevos popup, the full cheevos is displayed in a modal.'),
    '#default_value' => variable_get('cheevos_modal', TRUE),
    '#states' => array(
      'invisible' => array(
       ':input[name="cheevos_javascript"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['manually_add'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add A Cheevos to a User'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['manually_add']['user_find'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#autocomplete_path' => 'user/autocomplete',
  );

  $form['manually_add']['cheevos_find'] = array(
    '#type' => 'textfield',
    '#title' => t('Cheevos'),
    '#autocomplete_path' => 'admin/cheevos/autocomplete/cheevos',
  );

  $form['manually_add']['cheevos_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Cheevos'),
  );

  $form['cheevos_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
  );

  return $form;
}

/**
 * Submit handler.
 */
function cheevos_settings_form_submit($form, $form_state) {
  // Load the user id based on the value from the autocomplete.
  if (!empty($form_state['values']['user_find']) && !empty($form_state['values']['cheevos_find'])) {
    $uid = db_select('users', 'u')
      ->fields('u', array('uid'))
      ->condition('name', $form_state['values']['user_find'])
      ->execute()
      ->fetchField();

    // Check to see if the user already has the cheevos.
    $exists = db_select('cheevos_user', 'cu')
      ->fields('cu', array('cid'))
      ->condition('uid', $uid)
      ->condition('cid', $form_state['values']['cheevos_find'])
      ->execute()
      ->fetchField();

    // If the user does not have the cheevos add it.
    if (empty($exists)) {
      db_insert('cheevos_user')
        ->fields(array(
          'uid' => $uid,
          'cid' => $form_state['values']['cheevos_find'],
          'obtained' => time(),
        ))
        ->execute();
      drupal_set_message(t('The cheevos has been added to @user', array(
        '@user' => $form_state['values']['user_find'],
      )));
    }
  }

  // Set variables.
  variable_set('cheevos_label', $form_state['values']['cheevos_label']);
  variable_set('cheevos_plural', $form_state['values']['cheevos_plural']);
  variable_set('cheevos_plural_diff', $form_state['values']['cheevos_plural_diff']);
  variable_set('cheevos_javascript', $form_state['values']['cheevos_javascript']);
  variable_set('cheevos_position', $form_state['values']['cheevos_position']);
  if ($form_state['values']['cheevos_javascript'] == 0) {
    variable_set('cheevos_modal', FALSE);
  }
  else {
    variable_set('cheevos_modal', $form_state['values']['cheevos_modal']);
  }


  // Confirm settings were saved.
  drupal_set_message('Your settings have been saved.');
}

/**
 * Menu callback.
 *
 * Retrieve a JSON object containing autocomplete suggestions for cheevos.
 */
function cheevos_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_select('cheevos', 'c')
      ->fields('c', array('name', 'cid'))
      ->condition('name', db_like($string) . '%', 'LIKE')
      ->range(0, 10)
      ->execute();

    foreach ($result as $cheevos) {
      $matches[$cheevos->cid] = check_plain($cheevos->name);
    }
  }
  drupal_json_output($matches);
}

/**
 * Page to add Cheevos Entities.
 *
 * @todo Pass this through a proper theme function.
 */
function cheevos_add_page() {
  $controller = entity_ui_controller('cheevos');
  return $controller->addPage();
}

/**
 * Displays the list of available cheevos types for cheevos creation.
 *
 * @ingroup themeable.
 */
function theme_cheevos_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="cheevos-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer cheevos types')) {
      $output = '<p>' . t('Cheevos Entities cannot be added because you have not created any cheevos types yet. Go to the <a href="@create-cheevos-type">cheevos type creation page</a> to add a new cheevos type.', array('@create-cheevos-type' => url('admin/cheevos/cheevos_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No cheevos types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}

/**
 * Sets the breadcrumb for administrative cheevos pages.
 */
function cheevos_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('Cheevos'), 'admin/content/cheevos'),
  );

  drupal_set_breadcrumb($breadcrumb);
}
