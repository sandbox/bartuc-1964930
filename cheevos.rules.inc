<?php

/**
 * @file
 * Rules integration for the cheevos module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function cheevos_rules_condition_info() {
  return array(
    'cheevos_condition_comment_count' => array(
      'label' => t('Check Total Comment Posts'),
      'group' => t('Cheevos'),
      'parameter' => array(
        'cheevos_count' => array(
          'label' => t('How many?'),
          'type' => 'integer',
          'description' => t('How many times does a comment need to be added to fulfill this condition?'),
        ),
      )
    ),
    'cheevos_condition_node_count' => array(
      'label' => t('Check Total Node Posts'),
      'group' => t('Cheevos'),
      'parameter' => array(
        'node' => array('type' => 'node', 'label' => t('Content')),
        'cheevos_node' => array(
          'type' => 'list<text>',
          'label' => t('Content types'),
          'options list' => 'node_type_get_names',
          'description' => t('The content type(s) to check for.'),
          'restriction' => 'input',
        ),
        'cheevos_count' => array(
          'label' => t('How many?'),
          'type' => 'integer',
          'description' => t('How many times does this type need to be created to fulfill this condition?'),
          'restriction' => 'input',
        ),
        'cheevos_total' => array(
          'label' => t('Total node values?'),
          'type' => 'boolean',
          'description' => t('This will add the total of all nodes when calculating the amount of content created. For example, if you want to add the cheevos to a user that has created 25 articles, pages, or products you would leave this unchecked. If you want to award the cheevos to a user that has created a combination of 25 articles, pages, and products, you would check this option.'),
          'restriction' => 'input',
        ),
      )
    ),
    'cheevos_condition_nodes_row' => array(
      'label' => t('Check Nodes Created in a Row'),
      'group' => t('Cheevos'),
      'parameter' => array(
        'cheevos_node' => array(
          'type' => 'token',
          'label' => t('Content types'),
          'options list' => 'node_type_get_names',
          'description' => t('The content type to check for.'),
          'restriction' => 'input',
        ),
        'cheevos_count' => array(
          'label' => t('How many?'),
          'type' => 'integer',
          'description' => t('How many times does the content type need to be created in a row?'),
        ),
      )
    ),
    'cheevos_condition_nodes_timed' => array(
      'label' => t('Check Nodes Created (Timed)'),
      'group' => t('Cheevos'),
      'parameter' => array(
        'cheevos_node' => array(
          'type' => 'token',
          'label' => t('Content types'),
          'options list' => 'node_type_get_names',
          'description' => t('The content type to check for.'),
          'restriction' => 'input',
        ),
        'cheevos_time' => array(
          'label' => t('Time'),
          'type' => 'integer',
          'description' => t('In how much time should these nodes be created?'),
        ),
        'cheevos_granularity' => array(
          'label' => t('Granularity'),
          'type' => 'token',
          'options list' => 'cheevos_granularity_options',
          'description' => t('In how much time should these nodes be created? Use the granularity form below to select a measurement.'),
        ),
        'cheevos_count' => array(
          'label' => t('How many?'),
          'type' => 'integer',
          'description' => t('How many nodes need to be created in X amount of time?'),
        ),
      ),
    ),
    'cheevos_condition_has_cheevos' => array(
      'label' => t('Has Cheevos'),
      'group' => t('Cheevos'),
      'parameter' => array(
        'cheevos' => array(
          'type' => 'token',
          'label' => t('The Cheevos to check'),
          'options list' => 'cheevos_load_all',
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function cheevos_rules_action_info() {
  return array(
    'cheevos_add' => array(
      'label' => t('Add a Cheevos'),
      'base' => 'cheevos_action_assign_cheevos',
      'group' => t('Cheevos'),
      'access callback' => TRUE,
      'parameter' => array(
        'cheevos' => array(
          'type' => 'list<cheevos>',
          'label' => t('Cheevos to add'),
          'options list' => 'cheevos_load_all',
          'description' => t('Choose the cheevos in which you would like to add. You may have multiple selections.'),
        ),
      ),
    ),
    'cheevos_remove' => array(
      'label' => t('Remove a Cheevos'),
      'base' => 'cheevos_action_remove_cheevos',
      'group' => t('Cheevos'),
      'access callback' => TRUE,
      'parameter' => array(
        'cheevos' => array(
          'type' => 'list<cheevos>',
          'label' => t('Cheevos to remove'),
          'options list' => 'cheevos_load_all',
          'description' => t('Choose the cheevos in which you would like to have removed. You may have multiple selections.'),
        ),
      ),
    ),
  );
}

/**
 * Rules condition callback.
 *
 * @param condition
 *  The integer value to be met.
 */
function cheevos_condition_comment_count($condition) {
  // Select how many comments the current user has posted.
  $query = db_select('comment', 'c')
    ->fields('c', array('cid'))
    ->condition('c.uid', $GLOBALS['user']->uid);
  $count = $query->countQuery()
    ->execute()
    ->fetchField();

  // Return whether the condition was met or not.
  return $count == $condition ? TRUE : FALSE;
}

/**
 * Rules condition callback.
 *
 * @param param_node
 *  The node that waw selected as part of the condition.
 *
 * @param param_count
 *  The number of nodes that need to be created to satisfy the condition.
 *
 */
function cheevos_condition_nodes_row($param_node, $param_count) {
  // Determine if the last $condition nodes have been created in a row.
  $nodes = db_select('node', 'n')
    ->fields('n', array('type'))
    ->condition('n.uid', $GLOBALS['user']->uid)
    ->orderBy('created', 'DESC')
    ->range(0, $param_count)
    ->execute();

  $counter = 0;
  foreach ($nodes as $node) {
    if ($node->type == $param_node) {
      $counter++;
    }
  }

  // Return whether the condition was met or not.
  return $counter == $param_count ? TRUE : FALSE;
}

/**
 * Rules condition callback.
 *
 * @param param_node
 *  Any nodes that were selected as part of the condition.
 *
 * @param param_time
 *  The time set for the nodes.
 *
 * @param param_gran
 *  The granularity set for the nodes.
 *
 * @param param_count
 *  The number of nodes that are needed to meet this condition.
 */
function cheevos_condition_nodes_timed($param_node, $param_time, $param_gran, $param_count) {
  // Calculate the time difference.
  $timestamp = $param_time * $param_gran;

  // Grab the last few nodes' timestamps (based on $param_count).
  $results = db_select('node', 'n')
    ->fields('n', array('created'))
    ->condition('n.uid', $GLOBALS['user']->uid)
    ->condition('n.type', $param_node)
    ->orderBy('created', 'DESC')
    ->range(0, $param_count)
    ->execute();

  // Place each result in an array.
  $nodes = array();
  foreach ($results as $result) {
    $nodes[] = $result;
  }

  if (sizeof($nodes) >= 2) {
    $first_node = array_pop($nodes);
    $first_node = $first_node->created;
    $last_node = $nodes[0]->created;

    // Compare the values to see if they meet the rules condition.
    if (($last_node - $first_node) <= $timestamp) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Rules condition callback.
 *
 * @see cheevos_rules_action_info()
 *
 * @param content
 *  The node that was created.
 *
 * @param param_node
 *  Any nodes that were selected as part of the condition.
 *
 * @param param_count
 *  The number of nodes that need to be created to satisfy the condition.
 *
 * @param param_total
 *  Boolean value that determines if all content should be added together.
 */
function cheevos_condition_node_count($content, $param_node, $param_count, $param_total) {
  // Check to see if the type matches.
  if (in_array($content->type, $param_node)) {
    // Check to see if the number of posts have been satisfied.
    $db_or = db_or();
    foreach ($param_node as $type) {
      $db_or->condition('type', $type);
    }
    $query = db_select('node', 'n')
      ->fields('n', array('type'))
      ->condition('n.uid', $content->uid)
      ->condition($db_or);
    $query->addExpression('COUNT(*)');

    // If the param_total is set, we need to add all types.
    if (!empty($param_total)) {
      $count = $query->execute()
      ->fetchAssoc();

      return $count['expression'] == $param_count ? TRUE : FALSE;
    }
    else {
      $count = $query->groupBy('n.type')
        ->orderBy(2)
        ->execute()
        ->fetchAssoc();
      return $count['expression'] == $param_count ? TRUE : FALSE;
    }
  }
  return FALSE;
}

/**
 * Rules condition callback.
 *
 * @see cheevos_rules_action_info()
 *
 * @param cid
 *  The Cheevos ID of the selected achievement.
 */
function cheevos_condition_has_cheevos($cid) {
  $exists = db_select('cheevos_user', 'cu')
    ->fields('cu', array('uid'))
    ->condition('uid', $GLOBALS['user']->uid)
    ->condition('cid', $cid)
    ->execute()
    ->fetchField();

  return !empty($exists) ? TRUE : FALSE;
}

/**
 * Rules action callback.
 *
 * @see cheevos_rules_action_info()
 *
 * @param cheevos
 *  The Cheevos object of the selected achievements.
 */
function cheevos_action_assign_cheevos($cheevos) {
  // Run through each cheevos that was selected in the action.
  foreach ($cheevos as $item) {
    // Check to make sure the user doesn't already have the cheevos.
    $exists = db_select('cheevos_user', 'cu')
      ->fields('cu', array('cid'))
      ->condition('uid', $GLOBALS['user']->uid)
      ->condition('cid', $item->cid)
      ->execute()
      ->fetchField();

    if (empty($exists)) {
      // If the user doesn't have the cheevos, add it.
      db_insert('cheevos_user')
        ->fields(array(
          'uid' => $GLOBALS['user']->uid,
          'cid' => $item->cid,
          'obtained' => time(),
        ))
        ->execute();

      cheevos_display_cheevos($item);

      // Record each cheevos added to the user.
      watchdog('cheevos', t('The cheevos @cheevos was added to the user @user', array('@cheevos' => $item->name, '@user' => $GLOBALS['user']->name)));
    }
  }
}

/**
 * Rules condition callback.
 *
 * @see cheevos_rules_action_info()
 */
function cheevos_action_remove_cheevos() {
  // Run through each cheevos that was selected in the action.
  foreach ($cheevos as $item) {
    // Check to make sure the user has the cheevos being deleted.
    $exists = db_select('cheevos_user', 'cu')
      ->fields('cu', array('cid'))
      ->condition('uid', $GLOBALS['user']->uid)
      ->condition('cid', $item->cid)
      ->execute()
      ->fetchField();

    if (!empty($exists)) {
      // If the user doesn't have the cheevos, add it.
      db_delete('cheevos_user')
        ->condition('uid', $GLOBALS['user']->uid)
        ->condition('cid', $item->cid)
        ->execute();
      drupal_set_message(t('The cheevos has been removed from @user', array(
        '@user' => $GLOBALS['user']->name,
      )));
      // Record each cheevos added to the user.
      watchdog('cheevos', t('The cheevos @cheevos was removed from the user @user', array('@cheevos' => $item->name, '@user' => $GLOBALS['user']->name)));
    }
  }
}

/**
 * Rules function for building a granularity list.
 */
function cheevos_granularity_options() {
  return array(
    1 => t('Seconds'),
    60 => t('Minutes'),
    3600 => t('Hours'),
    86400 => t('Days'),
    2678400 => t('Months'),
    32140800 => t('Years'),
  );
}
