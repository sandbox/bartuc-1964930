
--------------------------------------------------------------------------------
                                 Cheevos
--------------------------------------------------------------------------------

Maintainer: Bryan Jones (Bartuc)

The Cheevos modules allows you to create achievements that users can earn by
completing different tasks, giving incentive to spend more time on your sites.

Project: http://drupal.org/project/cheevos

Installation
------------
Drush is the easiest way to install and enable the module.
  drush dl cheevos
  drush en cheevos

If you dont have Drush, do the following.
  Download the module from http://drupal.org/project/cheevos.
  Place the module in your site's module directory (usually sites/all/modules)
  Enable the module through the Drupal interface.

Configuration
-------------
Once installed, access the modules settings page at admin/cheevos/settings. From
here you will see a few options.

Cheevos Label: The label the user's will see for the Cheevos. Most people will
not know what a Cheevos is, so you have the option the change it to something
like "Achievement", "Reward", etc.

The Plural and singular: The checkbox asks whether the word can be used as both
plural and singular. For example, Cheevos can be either. You have one Cheevos.
You have twelve Cheevos. Uncheck this box to use a different plural name.

Plural Wording: Only shows up in the Plural and singular checkbox is not
checked. Place the plural version of the word here.

Display using Javascript: If checked the Cheevos will pop up for the user in
a designated corner of the user's screen. If unchecked, it will display the
message in a drupal_set_message() function.

Indicator Position: Determines where the Cheevos will appear.

Show Cheevos in modal: If checked, the Cheevos will be displayed in a modal.

Add a Cheevos to a User: Here you can manually add a Cheevos to a user. The
first text box will select the user. The second will select the Cheevos.

How to Use
----------
Cheevos work the same way nodes do. You must first create a type. When a type is
created you can then create a Cheevos for that type. Cheevos are entities so you
can add any additional field to it, such as difficulty, reward, etc.

Once a Cheevos is created, you can use Rules to create the conditions in which
the user obtains it. The action should always be "Add a Cheevos".

Not all Cheevos must be earned through Rules. You can create a Cheevos for tasks
such as "Find a bug" or even "Find a Database Exploit". Some Cheevos may be
important enough to warrant a manual addition. Some may have no way of being
added through rules.

Cheevos is fully integrated with Views. Two default Views are provided with the
module: My Cheevos and Letterboard. The My Cheevos View is enabled by default
and will show on the user's page. The Letterboard is disabled by default and
gives a list of the Users with the highest number of points.
