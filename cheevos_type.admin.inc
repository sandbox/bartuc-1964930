<?php

/**
 * @file
 * Cheevos type editing UI.
 */

/**
 * UI controller.
 */
class CheevosTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage cheevos entity types, including adding
		and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the cheevos type editing form.
 */
function cheevos_type_form($form, &$form_state, $cheevos_type, $op = 'edit') {

  if ($op == 'clone') {
    $cheevos_type->label .= ' (cloned)';
    $cheevos_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $cheevos_type->label,
    '#description' => t('The human-readable name of this cheevos type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($cheevos_type->type) ? $cheevos_type->type : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'cheevos_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this cheevos type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A short description about the Cheevos Type.'),
    '#default_value' => isset($cheevos_type->description) ? $cheevos_type->description : '',
  );

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 90,
  );

  $form['cheevos_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cheevos Options'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    '#group' => 'additional_settings',
  );

  $form['cheevos_options']['suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Suffix'),
    '#default_value' => isset($cheevos_type->suffix) ? $cheevos_type->suffix : '',
    '#maxlength' => 32,
    '#size' => 16,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save cheevos type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function cheevos_type_form_submit(&$form, &$form_state) {
  $cheevos_type = entity_ui_form_submit_build_entity($form, $form_state);
  $cheevos_type->save();
  $form_state['redirect'] = 'admin/cheevos/cheevos_types';
}

/**
 * Form API submit callback for the delete button.
 */
function cheevos_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/cheevos/cheevos_types/' . $form_state['cheevos_type']->type . '/delete';
}
