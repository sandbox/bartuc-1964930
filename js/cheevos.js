/**
 * @file
 * Javascript to show the popup in an elegant way.
 */
(function ($) {

  Drupal.theme.prototype.cheevos_modal = function () {
    var html = '';
    html += '<div id="ctools-modal" class="popups-box">';
    html += '  <div class="ctools-modal-content ctools-modal-cheevos-modal-content">';
    html += '    <span class="popups-close"><a class="close" href="#">' + Drupal.CTools.Modal.currentSettings.closeImage + '</a></span>';
    html += '    <div class="modal-scroll"><div id="modal-content" class="modal-content popups-body"></div></div>';
    html += '  </div>';
    html += '</div>';
    return html;
  }

  Drupal.behaviors.cheevosModule = {
    attach: function (context, settings) {
      // Position the cheevos.
      var position = $('.cheevos-popup').attr('class');
      if (typeof position !== "undefined") {

        // Find out where the popup should go.
        switch (Drupal.settings.cheevos.position) {
          case '0':
            $('.cheevos-popup').css('top', '0').css('left', 0);
            break;

          case '1':
            $('.cheevos-popup').css('top', '0').css('right', 0);
            break;

          case '2':
            $('.cheevos-popup').css('bottom', '0').css('left', 0);
            break;

          case '3':
            $('.cheevos-popup').css('bottom', '0').css('right', 0);
            break;

          default:
            $('.cheevos-popup').css('bottom', '0').css('left', 0);

        }
        $('.cheevos-popup').slideDown("slow");

        // Allow the user to click the cheevos away.
        $('.cheevos-exit').click(function() {
          $(this).hide();
          $(this).parent().slideUp();
        });
      }
    }
  };
})(jQuery);
